import util.tools as tools
import importlib
import os

class PluginManager(object):
	"""docstring for ClassName"""


	def __init__(self, pluginDir):

		self.pluginDir = pluginDir
		self.pluginDict = dict()
		self.pluginsTypeLine = []
		self.pluginsTypeFilterLine = []

		self.loadPlugins()

	def loadPlugins(self):
		self.pluginFileList = os.listdir(self.pluginDir)
		for pluginFile in self.pluginFileList:
			if not pluginFile.startswith("__"):
				pluginFile = pluginFile.split('.')[0]
				plugin_module = importlib.import_module(self.pluginDir[:-1] + '.' + pluginFile, '.')
				plugin_instance = plugin_module.Plugin()
				if plugin_instance.enabled:
					self.pluginDict[plugin_instance.name] = plugin_instance

					if plugin_instance.pluginType == "filter-line":
						self.pluginsTypeFilterLine.append(plugin_instance.name)
					if plugin_instance.pluginType == "line":
						self.pluginsTypeLine.append(plugin_instance.name)



	def pluginInfo(self,targetPlugin = ''):
		#return info of plugin by targetPlugin name, else returns all info
		if targetPlugin:
			info = self.pluginByName(targetPlugin).info()
			return info
		else:
			infos = []
			for pluginName in self.pluginDict:
				plugin = self.pluginByName(pluginName)
				infos.append(plugin.info())
			return infos

	def pluginByName(self,pluginName):
		return self.pluginDict[pluginName]

	def count(self):
		return len(self.pluginDict)

	def pluginList(self,pluginType = ''):
		if pluginType == "line":
			return self.pluginsTypeLine
		elif pluginType == "filter-line":
			return self.pluginsTypeFilterLine
		else:
			return self.pluginDict.keys()

	def resetAllTotals(self):
		for pluginName in self.pluginList():
			plugin = self.pluginByName(pluginName)
			plugin.resetTotals()
			plugin.resetPeriod()	

	def resetAllPeriod(self):
		for pluginName in self.pluginList():
			plugin = self.pluginByName(pluginName)
			plugin.resetPeriod()

	def getAllPeriod(self):
		info = []
		for pluginName in self.pluginList():
			plugin = self.pluginByName(pluginName)
			thisInfo = [plugin.name]
			thisInfo.append(plugin.getPeriod())
			info.append(thisInfo)
		return info

	def getAllTotals(self):
		info = []
		for pluginName in self.pluginList():
			plugin = self.pluginByName(pluginName)
			thisInfo = [plugin.name]
			thisInfo.append(plugin.getTotals())
			info.append(thisInfo)
		return info

	def lineFiltersPassed(self,line,tagline):
		#filters return in format: Passed(bool), [output,messages,here,in,list]
		passed = True
		info = []
		for pluginName in self.pluginList(pluginType = "filter-line"):
			plugin = self.pluginByName(pluginName)
			result = plugin.giveLineFilter(line,tagline)
			if not result[0]:
				passed = False
				info.append([plugin.name,result[1]])

		return passed, info

	#----------------

	def giveAllPluginsLine(self,line,tagline):
		for pluginName in self.pluginList(pluginType = "line"):
			plugin = self.pluginByName(pluginName)
			plugin.giveLine(line,tagline)

from . import constants
import datetime

class Logger(object):
	"""docstring for ClassName"""


	def __init__(self, mode ,location = "", **kwargs):
		#optional: timestamp_style, proc_id

		#"file","print","both"
		self.mainlog = constants.LOG_DIR + constants.MAIN_LOG_FILENAME
		self.mode = mode
		self.fileLocation = location

		self.procID = kwargs.pop("proc_ID","")

		self.timestamp_style = kwargs.pop("timestamp_style",constants.TIMESTAMP_LOG_STYLE)

	def set_file_location(self,location):
		self.fileLocation = location

	def set_proc_id(self,proc_id):
		self.procID = proc_id

	def write_main_log(self,text):
		file = open(self.mainlog, mode='a+')
		file.write(text + "\n")
		file.close()

	def write(self,message,**kwargs):
		#optional: proc_ID, tag
		now = datetime.date.strftime(datetime.datetime.now(), self.timestamp_style)
		tag = kwargs.pop("tag","")
		if tag:
			tagstring = "[" + tag + "]"
		else:
			tagstring = ''
		ID = kwargs.pop("proc_ID",self.procID)
		message = (now + " " + ID + tagstring + ": " + message)#.encode("utf-8", errors='ignore')

		self.__output(message)

	def __output(self,text):
		if self.mode == "file":
			file = open(self.fileLocation, mode='a+')
			file.write(text + "\n")
			file.close()
		elif self.mode == "print":
			print(text)
		elif self.mode == "both":
			file = open(self.fileLocation, mode='a+')
			file.write(text + "\n")
			file.close()
			print(text)

		self.write_main_log(text)


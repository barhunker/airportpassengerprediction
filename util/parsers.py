import re
import sys
import datetime
from . import logger
from . import constants


VERIFYDATA = True


def timestampToDatetime(timestamp):
    return datetime.datetime.strptime(timestamp, constants.TIMESTAMP_CHAT_PARSE_STYLE)

def datetimeToTimestamp(dt):
    return dt.strftime(constants.TIMESTAMP_CHAT_PARSE_STYLE)

def insideBrackets(text):
    patternInsideBrackets = re.compile("\[(.*)\]")
    patternInsideBracketsRe = re.search(patternInsideBrackets, text)
    if patternInsideBracketsRe:
        insides = patternInsideBracketsRe[1]
    else:
        insides = None

    return insides 

def params(text):
    patternParams = re.compile("\((.*)\)")
    patternParamsRe = re.search(patternParams, text)
    if patternParamsRe:
        insides = patternParamsRe[1]
    else:
        insides = None

    return insides 

def action(line):
    patternAction = re.compile(" action=(.*?), out=")

    actionRe = re.search(patternAction, line)
    if actionRe:
        action = actionRe[1]
    else:
        action = None

    if VERIFYDATA:
        pass
    #print(message_type)
    return action




def messageText(line):
    #returns None or message text

    #groups:
    #message= and leading quote
    #message text
    #end quote and comma WITH space
    #the rest of the string
    patternMessage = re.compile("(message=['\"])(.*?)(['\"], out=)")

    if line.startswith("Message("):
        messageTextRe = re.search(patternMessage, line)
        if messageTextRe:
            messageText = messageTextRe[2]

        if VERIFYDATA:
            pass
            #manual inspection
            #cleanPrint("========manual inspection=====================\n"+messageText+"\n\n"+line+"\n\n")

            #delete message and associated quotes, are there other quotes? (EXCLUDING HYPERLINKS)
            # withoutMessageText = re.sub(patternMessage, "OLDTEXTWASHERE",line)
            # countOfQuoteMarks = withoutMessageText.count("'") + withoutMessageText.count('"')
            # if countOfQuoteMarks != 0:
            #     if not messageText.startswith('http'):
            #         cleanPrint("more quote marks found after messagetext grab and not a hyperlink\n"+messageText+"\n"+withoutMessageText+"\n"+line+"\n")

        return messageText

    else:
        return None

def messageType(line):
    #calls mediaType and gives one simple answer to type
    message_type = ''
    if line.startswith("MessageService("):
        message_type = "service"
    else:
        media_type = mediaType(line)
        if media_type:
            message_type = media_type
        else:
            message_type = "text"

    if VERIFYDATA:
        #no type found and no text found
        if message_type == "text":
            if not messageText(line):
                cleanPrint("no message type found and no text found\n\n"+line+"\n")
    #print(message_type)
    return message_type

def mediaType(line):
    #returns None or mediaType (lowercase)
    patternMediaType = re.compile("(media=(\w+)\()")

    mediaTypeRe = re.search(patternMediaType, line)
    if mediaTypeRe:
        mediaType = mediaTypeRe[2]

        if VERIFYDATA:
            #starts with MessageMedia
            if not mediaType.startswith("MessageMedia"):
                cleanPrint("mediatype does not start with MessageMedia\n\n"+mediaType+"\n"+line+"\n")

        return mediaType.lower()
    else:
        return None

def insideParen(line):
    line = line.split("(")[1]
    line = line.split(")")[0]
    return line

def messageID(line):
    #returns id or 0 if failed
    patternID = re.compile('(?<=id=)(\d+)')
    reSearch = re.search(patternID, line)
    if reSearch:
        messageID = reSearch[1]
    else:
        messageID = 0

    if VERIFYDATA:
        #not int
        try:
            int(messageID)
        except Exception:
            cleanPrint("failed id grab\n\n"+line+"\n")

    return messageID

def fromID(line):
    patternFromID = re.compile('(?<=from_id=)(\d+)')
    from_id = re.search(patternFromID, line)[1]

    if VERIFYDATA:
        #not int
        try:
            int(from_id)
        except Exception:
            cleanPrint("failed from_id grab\n\n"+line+"\n")

    return from_id

def sent_timestamp(line):
    #assumes UTC
    patternDateNums = re.compile('(date=datetime.datetime\()(.+?)(, tzinfo)')
    datere = re.search(patternDateNums, line)[2]

    #get in form (y,m,d,h,m,s)
    splitdate = datere.split(',')
    if len(splitdate) != 6:
        splitdate.append(0)

    if VERIFYDATA:
        #arguments not 6(y,m,d,h,m,s)
        #split = datere.split(',')
        if len(splitdate) != 6:
            cleanPrint("timestamp size not 6, even after adding a 0 in seconds place\n\n"+line)

        #all arguments are int
        for item in splitdate:
            try:
                int(item)
            except Exception:
                cleanPrint("item in timestamp is not int\n\n"+line+"\n")

    year = int(splitdate[0])
    month = int(splitdate[1])
    day = int(splitdate[2])
    hour = int(splitdate[3])
    minute = int(splitdate[4])
    second = int(splitdate[5])

    timestamp = datetime.datetime(year, month, day, hour, minute, second)

    return timestamp

#move to tools
def cleanPrint(text):
    while False:
        non_bmp_map = dict.fromkeys(range(0x10000, sys.maxunicode + 1), 0xfffd)
        text = text.translate(non_bmp_map)
        print(text)
    else:

        errorlog = open("parse_errors.txt","a",encoding="UTF-8")
        errorlog.write(text + '\n')
        errorlog.close()
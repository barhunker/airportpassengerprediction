import datetime

#folders:
FILE_DIRECTORY = "_file/"

LOG_DIR = FILE_DIRECTORY + "log/"
GEN_DIR = FILE_DIRECTORY + "gen/"
DEBUG_DIR = FILE_DIRECTORY + "debug/"

CHAT_LOGS_DIR = GEN_DIR + "chat_logs/"
PARSED_LOGS_DIR = GEN_DIR + "parsed_logs/"
ANALYZED_LOGS_DIR = GEN_DIR + "analyzed_logs/"
SCRAPE_DIR = GEN_DIR + "scrape/"
INPUT_DIR = GEN_DIR + "input/"
PRICE_SCRAPE_DIR = GEN_DIR + "price_scrape/"

#plugin dir
ANALYZE_PLUGIN_DIR = "plugins_analyze/"

#files
TELEGRAM_GROUPS_CSV = "telegram_groups.csv"
CMC_DATABASE_CSV = "cmc_database.csv"
PRICE_SCRAPE_TARGETS = "price_scrape_targets.csv"
CHAT_DOWNLOADER_TARGETS = "chat_download_targets.csv"

#logfiles
MAIN_LOG_FILENAME = "birdeater.log"
CHAT_PARSE_FILENAME = "chat_parse.log"
CMC_PRICE_SCRAPE_LOG_FILENAME = "cmc_price.log"
CMC_DB_SCRAPE_LOG_FILENAME = "cmc_db_scrape.log"
TELEGRAM_GROUPS_LOG_FILENAME = "telegram_groups.log"
TELEGRAM_DOWNLOADER_FILENAME = "downloader.log"
ANALYZE_LOG_FILENAME = "analyze.log"

#CSV output delim
CSV_DELIM = "|^|"

#style
TIMESTAMP_FILE_STYLE = "%m-%d-%y--%H-%M-%S"
TIMESTAMP_LOG_STYLE = "%m/%d/%y %H:%M:%S"
TIMESTAMP_CHAT_PARSE_STYLE = "%Y-%m-%d %H:%M:%S"


#downloader settings 
#todo clean this
MAXMESSAGESATATIME = 2500
NEWESTAMOUNTCHECK = 100
MESSAGESCONSIDEREDNEWCUTOFF = 500
TIMETOWAITBETWEENDOWNLOADS = 120
MAXREPEATS = 4
MAXRUNS = 4
SLEEPAFTERMAXRUNS = 60*120

MIN_SECONDS_BETWEEN_DOWNLOADS = 60*30

#procIDs
PROC_ID_CHAT_DOWNLOADER = "log_downloader"
PROC_ID_CHAT_LOG_PARSER = "chat_log_parse"
PROC_ID_CMC_DB_SCRAPE = "cmc_db_scrape"
PROC_ID_CMC_PRICE_SCRAPE = "cmc_price_scrape"
PROC_ID_T_GROUP = "t_group_scrape"
PROC_ID_ANALYZE = "analyze"

#analysis
ANALYZE_TIME_PERIOD_SMALLEST = datetime.timedelta(days=1)
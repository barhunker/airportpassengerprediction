from . import constants
import os
import datetime


def csvPrint( f, *args):
		delim = constants.CSV_DELIM
		line = ''
		for item in args:
			if item:
				line+=(str(item) + delim)
			else:
				line+=('' + delim)
		f.write(line[:-(len(delim))]+"\n")

def csvPrintList( f, l):
		delim = constants.CSV_DELIM
		line = ''
		for item in l:
			line+=(str(item) + delim)
		f.write(line[:-(len(delim))]+"\n")

def pluginToCSVLine(timestamp, periodInfo, totalsInfo):
	info = [timestamp]
	for pluginOut in periodInfo:
		pluginName = pluginOut[0]
		for dictKey in pluginOut[1]:
			infoDict = pluginOut[1]
			value = infoDict[dictKey]
			info.append(value)
	for pluginOut in totalsInfo:
		pluginName = pluginOut[0]
		for dictKey in pluginOut[1]:
			infoDict = pluginOut[1]
			value = infoDict[dictKey]
			info.append(value)
	return(info)

def pluginToCSVTagline(periodInfo, totalsInfo):#timestamp ignored
	info = ["timestamp"]
	for pluginOut in periodInfo:
		pluginName = pluginOut[0]
		for dictKey in pluginOut[1]:
			itemName = "period-" + pluginName + "-" + dictKey
			info.append(itemName)
	for pluginOut in totalsInfo:
		pluginName = pluginOut[0]
		for dictKey in pluginOut[1]:
			itemName = "totals-" + pluginName + "-" + dictKey
			info.append(itemName)
	return(info)

def ifNotDirCreate(path):
	if not os.path.isdir(path):
		os.makedirs(path)

def cleanFilename(string):
	valid_chars = "-_.() QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890"
	return ''.join(c for c in string if c in valid_chars)

def logOutput(logfileLocation,procID,message,tag=''):
	now = datetime.date.strftime(datetime.datetime.now(), constants.TIMESTAMP_LOG_STYLE)
	if tag:
		tagstring = "[" + tag + "]"
	else:
		tagstring = ''
	message = (now + " " + procID + tagstring + ": " + message)#.encode("utf-8", errors='ignore')
	
	file = open(logfileLocation, mode='a+')
	file.write(message + "\n")
	file.close()
	print(message)

def parsedLineGetTag(line,tagline,tag):
	index = tagline.split(",").index(tag)
	return line.split(constants.CSV_DELIM)[index]

# def pluginOutputToFile(f, info):
# 	print()

# def cleanPrint(text):
# 	validLetters = "abcdefghijklmnopqrstuvwxyzRTYUIOPASDFGHJKLZXCVBNM1234567890 .?:,;\][{}|=-+_)(*&^%$#@!'\""
#     print( ''.join([char for char in text if char in validLetters]))
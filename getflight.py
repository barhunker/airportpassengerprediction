import math
from robobrowser import RoboBrowser
import datetime, os, dateparser, csv

OUTPUTDIR = 'output'
OUTFILE = "out.csv"
baseurl = "https://flightaware.com/live/airport/"
TARGETAIRPORT = "KSJC"
baseurl2 = "/enroute?;offset="

baseurl3 = ";sort=ASC;order=estimatedarrivaltime"
MAXPAGES = 5

def main():
        if not os.path.isdir(OUTPUTDIR):
                os.mkdir(OUTPUTDIR)
        

        
        #open robobrowser session and navigate to the target page
        browser = RoboBrowser(allow_redirects=False ,user_agent='Mozilla/5.0', history=True, parser='html.parser')
        #


        listingCount = 0

        h = open(OUTPUTDIR + '/' +OUTFILE,'w',encoding='utf-8')

        planeDict = dict()
        with open('data.csv', 'r') as f:
                tagLine = f.readline()
                for item in f:
                        itemSplit = item.split("|")
                        planeDict[itemSplit[1]] = itemSplit[3]

        #tagLine = planeList.pop(0)
        #['Count', 'Code', 'Aircraft Type', 'seats']
        #print(planeDict)

        csvPrint(h, "arrivalDatetime", "seatCount") 
        countsDict = dict()

        #populate dict
        now = datetime.datetime.now()
        tm = now + datetime.timedelta(days=1)
        
        startDT = now.replace(minute = 0, second = 0, microsecond = 0)
        while startDT < tm:
                #print(startDT)
                countsDict[startDT] = 0
                startDT = startDT + datetime.timedelta(minutes=15)

        for offsetMult in range(MAXPAGES):
                currentOffset = (offsetMult*20)
                targetpage = baseurl + TARGETAIRPORT + baseurl2 + str(currentOffset) + baseurl3
                browser.open(targetpage)

                table = browser.find_all("table")[2].find("table")

                found = table.find_all("tr")

                #smallrow1s + smallrow2s



                for item in found:
                        if "smallrow" in str(item):
                                tds = item.find_all("td")

                                planeType = tds[1].text
                                arrivalTime = tds[-1].text
                                arrivalTimeSplit = arrivalTime.replace(u'\xa0', ' ')
                                arrivalDatetime = dateparser.parse(arrivalTimeSplit, settings={'PREFER_DATES_FROM': 'past', 'RELATIVE_BASE': tm})
                                roundedDatetime =  datetime.datetime(arrivalDatetime.year, arrivalDatetime.month, arrivalDatetime.day, arrivalDatetime.hour,
                                        (arrivalDatetime.minute // 15) * 15)
                                #roundedDatetime = roundedDatetime.replace(minute = (arrivalDatetime.minute // 15) * 15)

                                if planeType:
                                        seatCount = int(planeDict[planeType])
                                        print(str(roundedDatetime) + "  " + planeType + "  " + str(seatCount))  
                                        if roundedDatetime in countsDict:
                                                countsDict[roundedDatetime] += seatCount
                                        #csvPrint(h, str(arrivalDatetime).replace('\n', ''), planeType.replace('\n', ''), str(seatCount).replace('\n', '')) 
                                else:
                                        pass
                                        #print("FAIL " + str(item))               
                
        #printdict to file
        for item in countsDict:
                print(str(item) + "   " + str(countsDict[item]))
                csvPrint(h, item, str(countsDict[item]))


        h.close()

def csvPrint( f, *args):
        delim = "|^|"
        line = ''
        for item in args:
                line+=(str(item) + delim)
        f.write(line[:-(len(delim))]+"\n")


#useless shit below here
def cleanFilename(string):
        valid_chars = "-_.() QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890"
        return ''.join(c for c in string if c in valid_chars)

def cleanDescription(string):
        return string.replace('\\n', '\n').replace('\\r', '\r').replace('\/', '/')

if __name__ == '__main__':
        main()
